console.log("Hello from JS");

// this is a single-line comment

/* this is a multi-line comment
comment 1
comment 2
comment 3
*/

//syntax: console.log(value/message);
// [DECLARING VARIABLES]
// tells our devices that a variable name is craeted and ready to store data.
// syntax: let/const desiredVariableName;

let myVariable;

//what is a variable?
	//used to contain/store data

//Benefits
	//makes it easier for us to associate information stored in our
	//devices to actual names about the information.

let clientName = "John Smith";
let contactNumber ="8888888";

//[Peeking inside a variable]
let greetings;

//3 Forms of Data Types:
	//Primitive - contains only a single value (strings, numbers, boolean)
	//Composite - contains multiple values (arrays, objects)
	//Special - null, undefined

//1. String
let country = "Philippines";
let province = 'Metro Manila';
//2. Number
let headcount = 26;
let grade = 98.7;
//3. Boolean
let isMarried = false;
let inGoodConduct = true;
//4. Null
let spouse = null;
console.log(country);


//5. Arrays - special kind of composite data type that is used to store mutiple values.
	// let's create a collection of all your subjects in bootcamp
let bootcampSubjects = ["HTML", "CSS", "Bootstrap", "Javascript"];
//display the output of the array inside the console
console.log(bootcampSubjects);

//Rule of Thumb when declaring array structures
// storing multiple data types inside an array is NOT recommended.
//in a context of programming this does not make any sense.
//an array should be a collection of data that describes a similar/single topic/subject.
let details = ["Keanu", "Reeves", 32, true];
console.log(details);

//Objects - are another special kind of composite data type that is used to mimic
	//represent a real world object/item.
	// using curly braces {}
	//used to create complex data that contains pieces of information
		//that are relevant to each other
	//every individual piece of code/info is called property of an object.
	//SYNTAX:
		/*let/const objectName = {
			key -> value
			propertyA: value,
			propertyB: value
		} 

		*/
let cellphone = {
	brand: 'Samsung',
	model: 'A12',
	color: 'Black',
	serialNo: 'AX12002122',
	isHomeCredit: true,
	features: ["Calling", "Texting", "Ringing", "5G"],
	price: 8000

}

//lets display the object inside the console.
console.log(cellphone);

let personName = "Michael";
console.log(personName);

//if you are going to reassign a new value for a variable,
//you no longer have to use the "let" again.
personName = "Jordan";
console.log(personName);

//concatenating string (+)
	//join, combine, link
let pet = "dog";
console.log("This is the initial value of var: " + pet);

pet = "cat";
console.log("This is the new value of the var: " + pet);

/*Constants
	- permanent, fixed, absolute
	- value assigned on a constant cannot be changed.
*/
const PI = 3.14;
console.log(PI);

const year = 12;
console.log(year);

const familyName = 'Dela Cruz';
console.log(familyName);

//reassigning a new value in a constant gives
	//Uncaught TypeError: Assignment to constant variable.
//you cant redeclare constant
